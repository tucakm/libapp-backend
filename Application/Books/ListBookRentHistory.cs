using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Books.Dto;
using Application.Errors;
using AutoMapper;
using Domain;
using Domain.Interfaces;
using MediatR;

namespace Application.Books
{
    public class ListBookRentHistory
    {
        public class Query : IRequest<List<BookRentHistoryDto>>
        {
            public Guid BookId { get; set; }
        }
        public class Handler : IRequestHandler<Query, List<BookRentHistoryDto>>
        {
           
            private readonly IMapper _mapper;
            private readonly IBookRepository _bookRepository;
            private readonly IRentRepository _rentRepository;
            public Handler(IBookRepository bookRepository, IRentRepository rentRepository,IMapper mapper)
            {
                this._bookRepository = bookRepository;
                this._rentRepository=rentRepository;          
                this._mapper = mapper;
            }

            public async Task<List<BookRentHistoryDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetBookByIdAsync(request.BookId);
                if (book == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Books = "Not found" });

                var bookHistory = await _rentRepository.GetBookHistoryAsync(request.BookId);

                return _mapper.Map<List<RentItem>, List<BookRentHistoryDto>>(bookHistory);
            }
        }
    }
}