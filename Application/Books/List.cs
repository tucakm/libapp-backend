using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Interfaces;
using MediatR;

namespace Application.Books
{
    public class List
    {
        public class Query : IRequest<List<Book>> { }

        public class Handler : IRequestHandler<Query, List<Book>>
        {
            private readonly IBookRepository _bookRepository;


            public Handler(IBookRepository bookRepository)
            {

                _bookRepository = bookRepository;
            }

            public async Task<List<Book>> Handle(Query request,
             CancellationToken cancellationToken)
            {
                var books = await _bookRepository.GetAllBooksAsync();
                return books;
            }
        }
    }
}