using System;
using Domain;

namespace Application.Books.Dto
{
    public class BookRentHistoryDto
    {
        public Guid RentItemId {get;set;}
        public Guid BookId { get; set; }
        public string BookTitle { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public DateTime? RentDate  { get; set; }
        public DateTime? ReturnDate { get; set; }
        public Status Status { get; set; }

    }
}