using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using Domain.Interfaces;
using FluentValidation;
using MediatR;

namespace Application.Books
{
    public class AddBookIntoPendingForUser
    {
        public class Command : IRequest
        {
            public Guid BookId { get; set; }
            public Guid UserId { get; set; }
            public int Quantity { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.BookId).NotEmpty();
                RuleFor(x => x.UserId).NotEmpty();
                RuleFor(x => x.Quantity).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command>
        {

            public readonly IBookRepository _bookRepository;
            public readonly IUserRepository _userRepository;
            public readonly IRentRepository _rentRepository;
            public Handler(IBookRepository bookRepository, IUserRepository userRepository, IRentRepository rentRepository)
            {
                this._userRepository = userRepository;
                this._bookRepository = bookRepository;
                this._rentRepository = rentRepository;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetBookByIdAsync(request.BookId);
                if (book == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Books = "Not found" });

                var user = await _userRepository.GetUserByIdAsync(request.UserId);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Users = "Not found" });

                //Check if user has any items allredy renting  
                if (await _rentRepository.CheckIfUserHaveRentsForDiffernetStatusAsync(user.Id, Status.Rendted))
                    throw new Exception("User needs to return allredy rented items before he can make another rent");

                //Check if user have allredy pending queue for rent
                if (!await _rentRepository.CheckIfUserHaveRentsForDiffernetStatusAsync(user.Id, Status.Pending))
                {
                    if (!await _rentRepository.CreatePendingRentForUserAsync(user))
                        throw new Exception("Couldnt add new renting queue for user");
                }
                //From user get user rent where status is pending
                var userRent = user.Rents.Where(x => (x.UserId == user.Id) && (x.Status == Status.Pending)).FirstOrDefault();



                if (!CheckQuantityOfBook(book, request.Quantity))
                    throw new Exception($"Not enouge books, avaible {book.AvaibleQuantity}");

                var rentItems = await _rentRepository.GetRentItemForBookAsync(userRent.Id, book.Id);

                if (rentItems != null)
                {
                    if (!await _rentRepository.IncreaseRentItemQuantityAsync(rentItems, book, request.Quantity))
                        throw new Exception("Problem saving rent item");
                }
                else
                {
                    if (!await _rentRepository.CreateRentItemAsync(userRent, book, request.Quantity))
                        throw new Exception("Problem saving rent item");
                }
                return Unit.Value;
            }
            private bool CheckQuantityOfBook(Book book, int quantity)
            {
                if (book.AvaibleQuantity >= quantity)
                    return true;
                else
                    return false;
            }
        }
    }

}