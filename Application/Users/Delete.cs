using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain.Interfaces;
using MediatR;

namespace Application.Users
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }
        public class Handler : IRequestHandler<Command>
        {
            public readonly IUserRepository _userRepository;
            public Handler(IUserRepository userRepository)
            {
                this._userRepository = userRepository;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {

                var user = await _userRepository.GetUserByIdAsync(request.Id);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { User = "Not found" });

                if (await _userRepository.DeleteUserAsync(user))
                    return Unit.Value;
                throw new Exception("Problem saving changes");
            }
        }
    }
}