using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using AutoMapper;
using Domain;
using Domain.Interfaces;
using MediatR;

namespace Application.Users
{
    public class Read
    {
        public class Query : IRequest<UserDto>
        {
            public Guid Id { get; set; }
        }
        public class Handler : IRequestHandler<Query, UserDto>
        {
            private readonly IMapper _mapper;
            public readonly IUserRepository _userRepository;
            public Handler(IUserRepository userRepository, IMapper mapper)
            {
                this._userRepository = userRepository;
                this._mapper = mapper;
            }
            public async Task<UserDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _userRepository.GetUserByIdAsync(request.Id);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "Not found" });

                return _mapper.Map<User, UserDto>(user);
            }
        }
    }
}