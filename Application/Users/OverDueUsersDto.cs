using System;

namespace Application.Users
{
    public class OverDueUsersDto
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid RentId { get; set; }
        public Guid RentItemId { get; set; }
        public Guid BookId { get; set; }
        public string BookTitle { get; set; }
        public DateTime? RentDate  { get; set; }
        public DateTime? ReturnDate { get; set; }
    }
}