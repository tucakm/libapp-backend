using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain.Interfaces;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Users
{
    public class Update
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public DateTime? BirthDay { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.BirthDay).NotEmpty().LessThan(DateTime.Now);

            }
        }

        public class Handler : IRequestHandler<Command>
        {
            public readonly IUserRepository _userRepository;
            public Handler(IUserRepository userRepository)
            {
                this._userRepository = userRepository;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {


                /* if (await _context.Users.Where(x => (x.FirstName == request.FirstName) && (x.LastName == request.LastName) && (x.BirthDay == request.BirthDay)).AnyAsync())
                     throw new RestException(HttpStatusCode.BadRequest, new { Users = "User with combination of First name, Last name and birthday allredy exists" });*/

                var user = await _userRepository.GetUserByIdAsync(request.Id);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "Not found" });
                if (await _userRepository.UpdateUserAsync(user.Id, request.FirstName, request.LastName, request.BirthDay))
                    return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }


    }
}