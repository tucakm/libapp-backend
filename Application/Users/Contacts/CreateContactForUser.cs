using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using FluentValidation;
using MediatR;
using Domain.Interfaces;

namespace Application.Users.Contacts
{
    public class CreateContactForUser
    {
        public class Command : IRequest
        {
            public Guid UserId { get; set; }
            public int Type { get; set; }
            public string Value { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {               
                RuleFor(x => x.Type).InclusiveBetween(0, 2);
                RuleFor(x => x.Value).NotEmpty();

            }
        }
        public class Handler : IRequestHandler<Command>
        {
            public readonly IUserRepository _userRepository;
            public Handler(IUserRepository userRepository)
            {
                _userRepository = userRepository;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _userRepository.GetUserByIdAsync(request.UserId);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { User = " Not found" });

                var userContact = new UserContacts
                {
                    ContactType = (ContactType)request.Type,
                    Value = request.Value
                };

                if (await _userRepository.CreateContactForUserAsync(user.Id, userContact))
                    return Unit.Value;
                throw new Exception("Problem saving changes");
            }
        }
    }
}