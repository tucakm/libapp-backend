using System;
using Domain;

namespace Application.Users.Contacts
{
    public class UserContactDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public ContactType ContactType { get; set; }
        public string Value { get; set; }
    }
}