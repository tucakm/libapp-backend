using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using Domain.Interfaces;
using MediatR;

namespace Application.Users
{
    public class ListUsers
    {
        public class Query : IRequest<List<UserDto>>
        {

        }
        public class Handler : IRequestHandler<Query, List<UserDto>>
        {
            private readonly IUserRepository _userRepository;
            private readonly IMapper _mapper;
            public Handler(IUserRepository userRepository, IMapper mapper)
            {
                this._userRepository = userRepository;
                this._mapper = mapper;
            }
            public async Task<List<UserDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var users = await _userRepository.GetAllUsersAsync();

                return _mapper.Map<List<User>, List<UserDto>>(users);
            }
        }
    }
}