using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Application.Rents.Dto;
using Application.Users.Contacts;

namespace Application.Users
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime BirthDay { get; set; }

        [JsonPropertyName("user contacts")]
        public ICollection<UserContactDto> UserContacts { get; set; }

        [JsonPropertyName("User rents")]
        public ICollection<RentDto> Rents { get; set; }
    }
}