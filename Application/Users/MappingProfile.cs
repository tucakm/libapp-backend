using Application.Users.Contacts;
using AutoMapper;
using Domain;

namespace Application.Users
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserContacts, UserContactDto>();
            CreateMap<RentItem, OverDueUsersDto>()
                .ForMember(x => x.RentItemId, o => o.MapFrom(o => o.Id))
                .ForMember(x => x.BookId, o => o.MapFrom(o => o.Book.Id))
                .ForMember(x => x.BookTitle, o => o.MapFrom(o => o.Book.Title))
                .ForMember(x => x.UserId, o => o.MapFrom(o => o.Rent.UserId))
                .ForMember(x => x.FirstName, o => o.MapFrom(o => o.Rent.User.FirstName))
                .ForMember(x => x.LastName, o => o.MapFrom(o => o.Rent.User.LastName))
                .ForMember(x => x.RentDate, o => o.MapFrom(o => o.Rent.TimeOfRent))
                .ForMember(x => x.ReturnDate, o => o.MapFrom(o => o.DateOfReturn));

        }
    }
}