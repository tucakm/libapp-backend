using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using Domain.Interfaces;
using MediatR;
using Persistence;

namespace Application.Users
{
    public class ListUsersByOverdueTime
    {
        public class Query : IRequest<List<OverDueUsersDto>>
        {

        }
        public class Handler : IRequestHandler<Query, List<OverDueUsersDto>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            private readonly IRentRepository _rentRepository;
            public Handler(IRentRepository rentRepository, IMapper mapper)
            {
                this._rentRepository=rentRepository;                
                this._mapper = mapper;
            }
            public async Task<List<OverDueUsersDto>> Handle(Query request, CancellationToken cancellationToken)
            {

                var usersByOverDue = await _rentRepository.GetRentsByOverDueTime();
                return _mapper.Map<List<RentItem>, List<OverDueUsersDto>>(usersByOverDue);
            }
        }
    }
}