using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using Domain.Interfaces;
using FluentValidation;
using MediatR;

namespace Application.Rents
{
    public class ReturnRentedItemsForUser
    {
        public class Command : IRequest
        {
            public Guid BookId { get; set; }
            public Guid UserId { get; set; }
            public int ReturnedQuantity { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.BookId).NotEmpty();
                RuleFor(x => x.UserId).NotEmpty();
                RuleFor(x => x.ReturnedQuantity).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command>
        {

            public readonly IBookRepository _bookRepository;
            public readonly IUserRepository _userRepository;
            public readonly IRentRepository _rentRepository;
            public Handler(IBookRepository bookRepository, IUserRepository userRepository, IRentRepository rentRepository)
            {
                this._userRepository = userRepository;
                this._bookRepository = bookRepository;
                this._rentRepository = rentRepository;
            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var book = await _bookRepository.GetBookByIdAsync(request.BookId);
                if (book == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Books = "Not found" });

                var user = await _userRepository.GetUserByIdAsync(request.UserId);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Users = "Not found" });
                   
                var rent = await _rentRepository.GetRentForUserAsync(user.Id,Status.Rendted);
                if (rent == null)
                    throw new RestException(HttpStatusCode.NotFound, new { Rent = "There is no rented items" });

                var rentItem = await _rentRepository.GetRentItemForBookNotReturnedAsync(rent.Id,book.Id);
                if (rentItem == null)
                    throw new RestException(HttpStatusCode.NotFound, new { RentItem = "There is no selected book rented by user" });
                //if everything passed remove element from collection
                var oldRentItem = rentItem;
                rent.RentItems.Remove(oldRentItem);
                rentItem.DateOfReturn = DateTime.Now;

                if (rentItem.Quantity > request.ReturnedQuantity)
                {

                    book.AvaibleQuantity += request.ReturnedQuantity;
                    rentItem.ReturnedQuantity = request.ReturnedQuantity;
                    //Create new rent item for not returned  books 
                    var newRentItem = new RentItem
                    {
                        Book = book,
                        Quantity = rentItem.Quantity - request.ReturnedQuantity,
                        Rent = rent
                    };
                    //add new item into collection
                    rent.RentItems.Add(newRentItem);
                }
                //If there is equals or more books returned, setting returned books to number of borowed books 
                else
                {
                    rentItem.ReturnedQuantity = rentItem.Quantity;                       
                    book.AvaibleQuantity += rentItem.Quantity ;
                }
                //
          
                //add changed rent item 
                rent.RentItems.Add(rentItem);

                //Check if all books are returned, we do that by checking every item in rent, if it has date of return, its returned. 
                if (!rent.RentItems.Where(x => x.DateOfReturn == null).Any())
                    rent.Status = Status.Returned;
               
                if (await _rentRepository.SaveChangesAsync())
                    return Unit.Value;

                throw new System.NotImplementedException();
            }
        }
    }
}