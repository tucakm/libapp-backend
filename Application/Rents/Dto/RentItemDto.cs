using System;

namespace Application.Rents.Dto
{
    public class RentItemDto
    {
        public Guid Id { get; set; }
        public Guid BookId { get; set; }
        public string BookTitle { get; set; }
        public int Quantity { get; set; }
        public int? ReturnedQuantity { get; set; }
        public DateTime? DateOfReturn { get; set; }

    }
}