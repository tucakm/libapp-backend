using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Application.Rents.Dto
{
    public class RentDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        public DateTime? TimeOfRent { get; set; }
        public DateTime? ExpectedReturnDate { get; set; }

        [JsonPropertyName("RentItems")]
        public ICollection<RentItemDto> RentItems { get; set; }
    }
}