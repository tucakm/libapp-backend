using AutoMapper;
using Domain;

namespace Application.Rents.Dto
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Rent, RentDto>();           
            CreateMap<RentItem, RentItemDto>()
                .ForMember(x => x.BookId, o => o.MapFrom(s => s.Book.Id));   
        }
    }
}