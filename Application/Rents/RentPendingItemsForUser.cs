using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using Domain.Interfaces;
using FluentValidation;
using MediatR;

namespace Application.Rents
{
    public class RentPendingItemsForUser
    {
        public class Command : IRequest
        {
            public Guid UserId { get; set; }
        }
        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.UserId).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<Command>
        {
            public readonly IRentRepository _rentRepository;
            public Handler(IRentRepository rentRepository)
            {
                this._rentRepository = rentRepository;

            }
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var rent = await _rentRepository.GetRentForUserAsync(request.UserId, Status.Pending);
                if (rent == null)
                   throw new RestException(HttpStatusCode.NotFound, new { Rent = "pending rent not found" });

                //change rent goup status to rented, set time of rent to now, and expected time of return is 30 days from now 
                if (await _rentRepository.ChangeRentStatusAsync(rent.Id, Status.Rendted, DateTime.Now, DateTime.Now.AddDays(30)))
                    return Unit.Value;

                throw new NotImplementedException("Problem with saving changes");
            }
        }
    }
}