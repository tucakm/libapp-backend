using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context)
        {
            this._context = context;
        }    
        public async Task<bool> CreateUserAsync(User user)
        {
            _context.Users.Add(user);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteUserAsync(User user)
        {
            _context.Remove(user);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetUserByIdAsync(Guid id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<bool> UpdateUserAsync(Guid olduserId, string firstName, string lastName, DateTime? BirthDay)
        {
            var user = await _context.Users.FindAsync(olduserId);
            user.FirstName = firstName ?? user.FirstName;
            user.LastName = lastName ?? user.LastName;
            user.BirthDay = BirthDay ?? user.BirthDay;
            return await _context.SaveChangesAsync() > 0;
        }
        public async Task<bool> CreateContactForUserAsync(Guid Userid, UserContacts contact)
        {
            var user = await _context.Users.FindAsync(Userid);
            user.UserContacts.Add(contact);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}