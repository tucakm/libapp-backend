using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Persistence.Repositories
{
    public class RentRepository : IRentRepository
    {
        private readonly DataContext _context;
        public RentRepository(DataContext context)
        {
            this._context = context;
        }

        public async Task<bool> AddRentItemToRent(RentItem rentItem,Rent rent)
        {            
            rent.RentItems.Add(rentItem);

            return await _context.SaveChangesAsync()>0;
        }

        public async Task<bool> ChangeRentStatus(Guid rentId, Status newStatus)
        {
            var rent= await _context.Rents.FindAsync(rentId);
            rent.Status=newStatus;
            return await _context.SaveChangesAsync()>0;
        }

        public async Task<bool> ChangeRentStatusAsync(Guid rentID, Status rentStatus, DateTime date,DateTime expectedReturnDate)
        {
            var rent= await _context.Rents.FindAsync(rentID);
            rent.Status=rentStatus;
            rent.TimeOfRent=date;
            rent.ExpectedReturnDate=expectedReturnDate;
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> CheckIfUserHaveRentsForDiffernetStatusAsync(Guid userId, Status rentStatus)
        {
            return await _context.Rents.Where(x => (x.UserId == userId) && (x.Status == rentStatus)).AnyAsync();
        }

        public async Task<bool> CreatePendingRentForUserAsync(User user)
        {
            var rent = new Rent
            {
                User = user,
                Status = Status.Pending
            };
            _context.Rents.Add(rent);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> CreateRentItemAsync(Rent rent, Book book, int quantity)
        {
            var rentItem = new RentItem
            {
                Book = book,
                Quantity = quantity,
                Rent = rent
            };
            book.AvaibleQuantity -= quantity;
            _context.RentItems.Add(rentItem);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<List<RentItem>> GetBookHistoryAsync(Guid bookId)
        {
            var bookHistory = await _context.RentItems.Where(x => (x.BookId == bookId)).OrderByDescending(x => x.DateOfReturn).ToListAsync();
            return bookHistory;
        }

        public async Task<Rent> GetRentForUserAsync(Guid userid, Status rentStatus)
        {
            return await _context.Rents.Where(x => (x.UserId == userid) && (x.Status == rentStatus)).FirstOrDefaultAsync();
        }

        public async Task<RentItem> GetRentItemForBookAsync(Guid rentId, Guid bookId)
        {
            return await _context.RentItems
                    .Where(x => (x.RentId == rentId) && (x.BookId == bookId)).FirstOrDefaultAsync();
        }

        public async Task<RentItem> GetRentItemForBookNotReturnedAsync(Guid rentId, Guid bookId)
        {
            return await _context.RentItems.Where(x => (x.BookId == bookId) && (x.RentId == rentId) && (x.DateOfReturn == null)).FirstOrDefaultAsync();
        }

        public async Task<List<RentItem>> GetRentsByOverDueTime()
        {
            return await _context.RentItems.Where(x => x.Rent.Status==Status.Returned && x.DateOfReturn > x.Rent.ExpectedReturnDate).OrderByDescending(x => x.DateOfReturn).ToListAsync();
        }

        public async Task<bool> IncreaseRentItemQuantityAsync(RentItem item, Book book, int quantity)
        {
            var rentItem= await _context.RentItems.FindAsync(item.Id);
            rentItem.Quantity += quantity;
            rentItem.Book.AvaibleQuantity -= quantity;
            return await _context.SaveChangesAsync()>0;
    
        }

        public async Task<bool> RemoveRentItemAsync(RentItem rentitem)
        {
            var rent = await _context.Rents.FindAsync(rentitem.Rent.Id);
            rent.RentItems.Remove(rentitem);

            return await _context.SaveChangesAsync()>0;
        }

        public async Task<bool> SaveChangesAsync()
        {
           return await _context.SaveChangesAsync()>0;
        }

        public async Task<int?> SumOfReturnedSameBooksBy(Guid bookId, Guid rentId)
        {
            return await _context.RentItems.Where(x => (x.BookId == bookId) && (x.RentId == rentId) && (x.DateOfReturn != null)).Select(x => x.ReturnedQuantity).SumAsync();
        }
    }
}