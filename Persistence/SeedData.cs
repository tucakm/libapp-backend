using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;

namespace Persistence
{
    public class SeedData
    {

        public static async Task Seed(DataContext context)
        {
            if (!context.Books.Any())
            {
                var Books = new List<Book>
                {
                    new Book
                    {
                        Title="Eloquent JavaScript, Second Edition",
                        Author="Marijn Haverbeke",
                        Quantity=3,
                        AvaibleQuantity=3
                        

                    },
                     new Book
                    {
                        Title="Learning JavaScript Design Patterns",
                        Author="Addy Osmani",
                        Quantity=5,
                        AvaibleQuantity=5

                    },
                     new Book
                    {
                        Title="Speaking JavaScript",
                        Author="Axel Rauschmayer",
                        Quantity=6,
                        AvaibleQuantity=6

                    },
                     new Book
                    {
                        Title="Programming JavaScript Applications",
                        Author="Eric Elliott",
                        Quantity=3,
                        AvaibleQuantity=3


                    },
                     new Book
                    {
                        Title="The Definitive Guide for JavaScript Developers",
                        Author="Nicholas C. Zakas",
                        Quantity=1,
                        AvaibleQuantity=1

                    },
                    new Book
                    {
                        Title="You Don't Know JS",
                        Author="Kyle Simpson",
                        Quantity=1,
                        AvaibleQuantity=1

                    },
                    new Book
                    {
                        Title="Designing Evolvable Web APIs with ASP.NET",
                        Author="Glenn Block, et al.",
                        Quantity=1,
                        AvaibleQuantity=1
                    }


                };
                context.Books.AddRange(Books);
                await context.SaveChangesAsync();
            }
        }
    }
}