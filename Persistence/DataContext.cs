using Domain;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserContacts> UserContacts { get; set; }

        public DbSet<Rent> Rents { get; set; }
        public DbSet<RentItem> RentItems { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserContacts>()
                .HasOne(uc => uc.User)
                .WithMany(u => u.UserContacts)
                .HasForeignKey(uc => uc.UserId);

            builder.Entity<RentItem>()
                .HasOne(ri => ri.Rent)
                .WithMany(r => r.RentItems)
                .HasForeignKey(ri => ri.RentId);

            builder.Entity<Rent>()
                .HasOne(r => r.User)
                .WithMany(u=>u.Rents)
                .HasForeignKey(r => r.UserId);
                
        }
    }
}