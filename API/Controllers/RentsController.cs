using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Rents;
using Application.Users;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class RentsController : BaseController
    {
        [HttpPost("rentpendingitems")]
        public async Task<ActionResult<Unit>> RentPendingItems(RentPendingItemsForUser.Command command)
        {
            return await Mediator.Send(command);
        }
        [HttpPost("returnrenteditems")]
        public async Task<ActionResult<Unit>> ReturnRentedItems(ReturnRentedItemsForUser.Command command)
        {
            return await Mediator.Send(command);
        }
        [HttpGet("usersbyoverduetime")]

        public async Task<ActionResult<List<OverDueUsersDto>>> UsersByOverdueTime()
        {
            return await Mediator.Send(new ListUsersByOverdueTime.Query());
        }
    }
}