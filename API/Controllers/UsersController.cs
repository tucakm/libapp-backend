using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Users;
using Application.Users.Contacts;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class UsersController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Update(Guid id, Update.Command command)
        {
            command.Id = id;
            return await Mediator.Send(command);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> Read(Guid id)
        {

            return await Mediator.Send(new Read.Query { Id = id });
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDto>>> GetUserList()
        {
            return await Mediator.Send(new ListUsers.Query());
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id)
        {

            return await Mediator.Send(new Delete.Command { Id = id });
        }
        [HttpPost("{id}/addcontact")]
        public async Task<ActionResult<Unit>> CreateContact(Guid id,CreateContactForUser.Command command)
        {
            command.UserId=id;
            return await Mediator.Send(command);
        }

    }
}