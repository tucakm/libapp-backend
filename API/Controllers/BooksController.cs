using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Books;
using Application.Books.Dto;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class BooksController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<Book>>> List()
        {
            return await Mediator.Send(new List.Query());
        }

        [HttpPost("addtopending")]
        public async Task<ActionResult<Unit>> AddToPending(AddBookIntoPendingForUser.Command command)
        {
            return await Mediator.Send(command);
        }
        [HttpGet("{id}/renthistory")]           
     
        public async Task<ActionResult<List<BookRentHistoryDto>>> BookRentHistory(Guid id)
        {
            return await Mediator.Send(new ListBookRentHistory.Query { BookId = id });
        }
    }
}