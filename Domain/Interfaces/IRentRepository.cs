using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRentRepository
    {
         Task<List<RentItem>> GetBookHistoryAsync(Guid bookId);
         Task<bool> CheckIfUserHaveRentsForDiffernetStatusAsync(Guid userId,Status rentStatus);
         Task<Rent> GetRentForUserAsync(Guid userid,Status rentStatus);
         Task<bool> CreatePendingRentForUserAsync(User user);
         Task<RentItem> GetRentItemForBookAsync(Guid rentId,Guid bookId);
         Task<RentItem> GetRentItemForBookNotReturnedAsync(Guid rentId,Guid bookId);
         Task<bool> IncreaseRentItemQuantityAsync(RentItem item,Book book, int quantity);
         Task<bool> CreateRentItemAsync(Rent rent, Book book, int quantity);

         Task<bool> ChangeRentStatusAsync(Guid rentID,Status rentStatus,DateTime date,DateTime expectedReturnDate);
         Task<bool> RemoveRentItemAsync(RentItem rentitem);

         Task<bool> AddRentItemToRent(RentItem rentItem,Rent rentId);
         Task<int?> SumOfReturnedSameBooksBy(Guid bookId,Guid rentId);
         Task<bool> ChangeRentStatus(Guid rentId,Status newStatus);

         Task<bool> SaveChangesAsync();

         Task<List<RentItem>> GetRentsByOverDueTime();
    }
}