using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUserRepository
    {
        Task<bool> CreateUserAsync(User user);
        Task<bool> UpdateUserAsync(Guid olduserId,string firstName,string lastName,DateTime? BirthDay);
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(Guid id);
        Task<bool> DeleteUserAsync(User user);

        Task<bool> CreateContactForUserAsync(Guid Userid,UserContacts contact);
   
    }
}