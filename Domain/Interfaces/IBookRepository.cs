using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IBookRepository
    {
         Task<List<Book>> GetAllBooksAsync();
         Task<Book> GetBookByIdAsync(Guid id);
    }
}