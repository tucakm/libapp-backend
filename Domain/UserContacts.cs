using System;

namespace Domain
{
    public class UserContacts
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public virtual User User {get;set;}
        public ContactType ContactType { get; set; }
        public string Value { get; set; }

    }

    public enum ContactType
    {
        Email,
        Phone,
        Fax,

    }
}