using System;

namespace Domain
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }        
        public int Quantity { get; set; }
        public int AvaibleQuantity { get; set; }
    }
}