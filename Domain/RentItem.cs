using System;

namespace Domain
{
    public class RentItem
    {
        public Guid Id { get; set; }
        public Guid BookId { get; set; }
        public virtual Book Book { get; set; }
        public int Quantity { get; set; }
        public int? ReturnedQuantity { get; set; }
        public DateTime? DateOfReturn { get; set; }
        public Guid RentId { get; set; }
        public virtual Rent Rent { get; set; }

    }
}