using System;
using System.Collections.Generic;

namespace Domain
{
    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public virtual ICollection<UserContacts> UserContacts { get; set; }
        public virtual ICollection<Rent> Rents { get; set; }
    }
}