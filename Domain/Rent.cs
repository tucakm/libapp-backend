using System;
using System.Collections.Generic;

namespace Domain
{
    public class Rent
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<RentItem> RentItems { get; set; }
        public DateTime? TimeOfRent { get; set; }
        public DateTime? ExpectedReturnDate { get; set; }
        public Status Status { get; set; }

    }
    public enum Status
    {
        Pending,
        Rendted,
        Returned
    }

}